# This file is Copyright (c) 2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2019 Benjamin Herrenschmidt <benh@ozlabs.org>
# This file is Copyright (c) 2020-2021 Raptor Engineering <sales@raptorengineering.com>
# License: BSD

import os

from migen import *

from litex import get_data_mod
from litex.soc.interconnect import wishbone
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.soc.cores.cpu import CPU


CPU_VARIANTS = ["standard"]

class XICSSlave(Module, AutoCSR):
    def __init__(self, platform, icp_bus, ics_bus, variant="standard"):
        self.variant    = variant

        self.icp_bus    = icp_bus
        self.ics_bus    = ics_bus

class LibreSoC(CPU):
    name                 = "libre_soc"
    human_name           = "Libre-SoC"
    variants             = CPU_VARIANTS
    data_width           = 64
    endianness           = "little"
    gcc_triple           = ("powerpc64le-linux", "powerpc64le-linux-gnu")
    linker_output_format = "elf64-powerpcle"
    nop                  = "nop"
    io_regions           = {0xc0000000: 0x10000000} # origin, length

    @property
    def mem_map(self):
        return {"csr": 0xc0000000}

    @property
    def gcc_flags(self):
        flags  = "-m64 "
        flags += "-mabi=elfv2 "
        flags += "-msoft-float "
        flags += "-mno-string "
        flags += "-mno-multiple "
        flags += "-mno-vsx "
        flags += "-mno-altivec "
        flags += "-mlittle-endian "
        flags += "-mstrict-align "
        flags += "-fno-stack-protector "
        flags += "-mcmodel=small "
        flags += "-D__libresoc__ "
        return flags

    def __init__(self, platform, variant="standard"):
        self.platform     = platform
        self.variant      = variant
        self.reset        = Signal()
        self.wb_insn      = wb_insn = wishbone.Interface(data_width=64, adr_width=29)
        self.wb_data      = wb_data = wishbone.Interface(data_width=64, adr_width=29)
        self.periph_buses = [wb_insn, wb_data]
        self.memory_buses = []
        self.interrupt    = Signal(16)

        self.icp_bus    = icp_bus    = wishbone.Interface(data_width=32, adr_width=12)
        self.ics_bus    = ics_bus    = wishbone.Interface(data_width=32, adr_width=12)

        # Bus endianness handlers
        self.icp_dat_w = Signal(32)
        self.icp_dat_r = Signal(32)
        self.comb += self.icp_dat_w.eq(icp_bus.dat_w if self.endianness == "big" else reverse_bytes(icp_bus.dat_w))
        self.comb += icp_bus.dat_r.eq(self.icp_dat_r if self.endianness == "big" else reverse_bytes(self.icp_dat_r))
        self.ics_dat_w = Signal(32)
        self.ics_dat_r = Signal(32)
        self.comb += self.ics_dat_w.eq(ics_bus.dat_w if self.endianness == "big" else reverse_bytes(ics_bus.dat_w))
        self.comb += ics_bus.dat_r.eq(self.ics_dat_r if self.endianness == "big" else reverse_bytes(self.ics_dat_r))

        # # #

        self.cpu_params = dict(
            # Clock / Reset
            i_clk              = ClockSignal(),
            i_rst              = ResetSignal() | self.reset,

            # Monitoring / debugging
            i_pc_i             = 0,
            i_pc_i_ok          = 0,
            i_core_bigendian_i = 0,
            #o_busy_o          =,
            #o_memerr_o        =,
            #o_pc_o            =,

            # Wishbone instruction bus
            i_ibus__dat_r      = wb_insn.dat_r,
            i_ibus__ack        = wb_insn.ack,

            o_ibus__adr        = wb_insn.adr,
            o_ibus__dat_w      = wb_insn.dat_w,
            o_ibus__cyc        = wb_insn.cyc,
            o_ibus__stb        = wb_insn.stb,
            o_ibus__sel        = wb_insn.sel,
            o_ibus__we         = wb_insn.we,

            # Wishbone data bus
            i_dbus__dat_r      = wb_data.dat_r,
            i_dbus__ack        = wb_data.ack,

            o_dbus__adr        = wb_data.adr,
            o_dbus__dat_w      = wb_data.dat_w,
            o_dbus__cyc        = wb_data.cyc,
            o_dbus__stb        = wb_data.stb,
            o_dbus__sel        = wb_data.sel,
            o_dbus__we         = wb_data.we,

            # Debug bus
            i_dmi_addr_i       = 0,
            i_dmi_din          = 0,
            #o_dmi_dout        =,
            i_dmi_req_i        = 0,
            i_dmi_we_i         = 0,
            #o_dmi_ack_o       =,

            # Interrupt controller
            i_int_level_i      = self.interrupt,

            # ICP
            # Wishbone bus
            o_icp_wb__dat_r   = self.icp_dat_r,
            o_icp_wb__ack     = self.icp_bus.ack,

            i_icp_wb__adr     = self.icp_bus.adr,
            i_icp_wb__dat_w   = self.icp_dat_w,
            i_icp_wb__cyc     = self.icp_bus.cyc,
            i_icp_wb__stb     = self.icp_bus.stb,
            i_icp_wb__sel     = self.icp_bus.sel,
            i_icp_wb__we      = self.icp_bus.we,

            # ICS
            # Wishbone bus
            o_ics_wb__dat_r   = self.ics_dat_r,
            o_ics_wb__ack     = self.ics_bus.ack,

            i_ics_wb__adr     = self.ics_bus.adr,
            i_ics_wb__dat_w   = self.ics_dat_w,
            i_ics_wb__cyc     = self.ics_bus.cyc,
            i_ics_wb__stb     = self.ics_bus.stb,
            i_ics_wb__sel     = self.ics_bus.sel,
            i_ics_wb__we      = self.ics_bus.we,
        )

        # add verilog sources
        self.add_sources(platform)

        # add XICS controller
        self.add_xics()

    def set_reset_address(self, reset_address):
        assert not hasattr(self, "reset_address")
        self.reset_address = reset_address
        assert reset_address == 0x00000000

    def add_xics(self):
        self.submodules.xics = XICSSlave(
            platform = self.platform,
            variant = self.variant,
            icp_bus = self.icp_bus,
            ics_bus = self.ics_bus)

    @staticmethod
    def add_sources(platform):
        cdir = os.path.dirname(__file__)
        platform.add_source(os.path.join(cdir, "libresoc.v"))

    def do_finalize(self):
        self.specials += Instance("test_issuer", **self.cpu_params)
